# ROI Revolution Developer Solution

# Instructions
## Using Rails application
* start the rails application
* goto homepage
* follow the on-screen Instructions (included below)

### Instructions: AdWords to JSON Convertor

To process the file:

* Enter the account name
* Choose the file you wish to process
* Click the `Create Account` button

The account will be created and will appear in the list of accounts available.

* To view the json output, click the `show` icon
* To download the json file, click the `download` icon
* To delete the account, click the `delete` icon

## Using rake task
The convertor can also be run as a rake task

```
rake convert_adwords_to_json account='sample' input='input.tsv' output='output.json'
```

# Models used
* Account
  * name
  * has_many campaigns
* Campaigns
  * belongs_to account
  * name
  * daily_budget,
  * status,
  * has_many add_groups
* AdGroup
  * belongs_to campaign
  * name
  * max_cpc
  * status
  * has_many Ads
  * has_many Keywords
* Ad
  * belongs_to ad_group
  * headline
  * description_line1
  * description_line2
  * display_url
  * final_url
  * status
* Keyword
  * belongs_to ad_group
  * keyword
  * type
  * first_page_bid
  * status


# ROI Revolution Developer Challenge

## Problem

Your company has been using TSV files to store their Google AdWords Account structure. They recently found a tool that can display this data, but the tool requires data to be formatted using [JSON](http://www.json.org/). Your manager has tasked you with creating a tool that will take files in their current TSV format and output a file that contains the same data in a JSON-formatted file.

## Background Knowledge

### Structure of an AdWords Account

AdWords accounts are structured as follows:

* An account can have multiple `Campaigns`
* A `Campaign` can have multiple `Ad Groups`
* An `Ad Group` can have multiple `Ads`
* An `Ad Group` can have multiple `Keywords`

![adwords_structure](http://media.tumblr.com/d9fc9502c82733250b2a780fe796d291/tumblr_inline_mmy11fi9FB1qz4rgp.png)

## Requirements

* This can be be written in your language of choice
* The solution should leverage the object oriented programming paradigm
* We expect each type of AdWords element to be represented by an object
* Instructions for how to use your solution should be included with your submission

## Expected Output

### Format

The output should be a JSON representation of the data matching the following format:

```json
{
  "campaigns": [{
    "name": "campaign1",
    "daily_budget": "1",
    "status": "paused",
    "ad_groups": [
      {
        "name": "adgroup1",
        "max_cpc": "1",
        "status": "paused",
        "ads": [
          {
            "headline": "headline",
            "description_line1": "line1",
            "description_line2": "line2",
            "display_url": "www.example.com",
            "final_url": "www.example.com",
            "status": "paused"
          }
        ],
        "keywords": [
          {
            "keyword": "keyword",
            "type": "phrase",
            "first_page_bid": "1",
            "status": "paused"
          }
        ]
      }
    ]
  }]
}
```

The attached `output.json` demonstrates this format for the data from `input.tsv`

### Ordering

To guarantee your output lines up with the expected output, elements should be ordered alphabetically by "name"
  * For `Campaign` use the `Campaign` field for the name
  * For `Ad Group`use the `Ad Group` field for the name
  * For `Ad` use the `Headline` field for the name
    * In the case of duplicate `Headline`s use `Description Line 1`
    * In the case of duplicate `Description Line 1`s use `Description Line 2`
  * For `Keyword` use the `Keyword` field for the name
    * In the case of duplicate `Keyword`s use `Criterion Type`

## Sample Data

### Inputs

Attached is a TSV representing your client's account structure, `input.tsv`. Your solution should allow for us to use other similarly formatted TSV files as input as well.

### Output

Also attached is `output.json`. This is the JSON representation of the data in `input.tsv`. Your solution is expected to match this output exactly.

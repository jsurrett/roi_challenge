FactoryGirl.define do
  factory :ad do
    ad_group nil
    headline "MyString"
    description_line1 "MyString"
    description_line2 "MyString"
    display_url "MyString"
    final_url "MyString"
    status "MyString"
  end
end

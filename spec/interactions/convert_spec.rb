require 'rails_helper'

describe Convert do
  describe 'execute' do
    let(:account_name) { 'test' }
    let(:old_input_file) { "#{Rails.root}/spec/input_files/ad_words.tsv" }
    let(:new_input_file) { "#{Rails.root}/spec/input_files/new_input.tsv" }
    let(:output_file) { "#{Rails.root}/tmp/downloads/ad_words_output.json" }
    let(:answer_file) { "#{Rails.root}/spec/output_files/ad_words.json" }

    it 'creates a json file with the correct format given old format' do
      Convert.run!(
        account_name: account_name,
        input_file: old_input_file,
        output_file: output_file
      )
      expect(File.read(output_file)).to eql(File.read(answer_file))
    end

    it 'creates a json file with the correct format given new format' do
      Convert.run!(
        account_name: account_name,
        input_file: new_input_file,
        output_file: output_file
      )
      expect(File.read(output_file)).to eql(File.read(answer_file))
    end
  end
end

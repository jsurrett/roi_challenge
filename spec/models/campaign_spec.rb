require 'rails_helper'

RSpec.describe Campaign, type: :model do
  # Associations
  it { should belong_to(:account) }
  it { should have_many(:ad_groups).dependent(:destroy) }

  # Validations
  it { should validate_presence_of(:account) }
  it { should validate_presence_of(:name) }
  it { should validate_numericality_of(:daily_budget).is_greater_than_or_equal_to(0) }
  it { should validate_presence_of(:status) }
end

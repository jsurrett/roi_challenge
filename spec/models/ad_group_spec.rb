require 'rails_helper'

RSpec.describe AdGroup, type: :model do
  # Associations
  it { should belong_to(:campaign) }
  it { should have_many(:ads).dependent(:destroy) }
  it { should have_many(:keywords).dependent(:destroy) }

  # Validations
  it { should validate_presence_of(:campaign) }
  it { should validate_presence_of(:name) }
  it { should validate_numericality_of(:max_cpc).is_greater_than_or_equal_to(0) }
  it { should validate_presence_of(:status) }
end

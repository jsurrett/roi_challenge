require 'rails_helper'

RSpec.describe Account, type: :model do
  # Associations
  it { should have_many(:campaigns).dependent(:destroy) }

  # Validations
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
end

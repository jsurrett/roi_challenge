require 'rails_helper'

RSpec.describe Keyword, type: :model do
  # Associations
  it { should belong_to(:ad_group) }

  # Validations
  it { should validate_presence_of(:ad_group) }
  it { should validate_presence_of(:keyword) }
  it { should validate_presence_of(:criterion_type) }
  it { should validate_numericality_of(:first_page_bid).is_greater_than_or_equal_to(0) }
  it { should validate_presence_of(:status) }
end

require 'rails_helper'

RSpec.describe Ad, type: :model do
  # Associations
  it { should belong_to(:ad_group) }

  # Validations
  it { should validate_presence_of(:ad_group) }
  it { should validate_presence_of(:headline) }
  it { should validate_presence_of(:description_line1) }
  it { should validate_presence_of(:description_line2) }

  it { should allow_value('http://foo.com', 'http://bar.com/baz').for(:display_url) }
  it { should_not allow_value('asdfjkl').for(:display_url) }

  it { should allow_value('http://foo.com', 'http://bar.com/baz').for(:final_url) }
  it { should_not allow_value('asdfjkl').for(:final_url) }

  it { should validate_presence_of(:status) }
end

require 'rails_helper'

feature 'Account crud' do
  let(:input_file) { "#{Rails.root}/spec/input_files/ad_words.tsv" }
  let(:output_file) { "#{Rails.root}/spec/output_files/ad_words.json" }

  scenario 'creating an acount', js: true do
    when_the_account_index_page_is_visited
    then_a_bootstrap_well_with_instructions_is_visible
    and_a_form_to_create_an_account_is_visible

    when_the_form_is_submitted
    then_a_message_indicating_account_was_created_is_visible
    and_the_account_is_listed_in_the_table_of_accounts

    when_the_show_button_is_clicked
    then_the_file_is_displayed

    when_the_download_button_is_clicked
    then_the_file_is_downloaded

    when_the_delete_button_is_clicked
    then_a_message_indicating_account_was_deleted_is_visible
    and_an_empty_table_of_accounts_is_visible
  end

  def when_the_account_index_page_is_visited
    visit accounts_path
  end

  def then_a_bootstrap_well_with_instructions_is_visible
    expect(find('#instructions')).to have_content 'Instructions'
  end

  def and_a_form_to_create_an_account_is_visible
    expect(find('#account_entry_form')).to have_field(:account_name)
    expect(find('#account_entry_form')).to have_field(:account_file)
  end

  def when_the_form_is_submitted
    fill_in 'account_name', with: 'testing'
    find('#account_entry_form').attach_file(:account_file, input_file)
    click_button 'Create Account'
  end

  def then_a_message_indicating_account_was_created_is_visible
    expect(find('#messages')).to have_content 'successfully created'
  end

  def and_the_account_is_listed_in_the_table_of_accounts
    expect(find('#accounts_list')).to have_content 'testing'
  end

  def when_the_show_button_is_clicked
    find('#accounts_list .fa-eye').click
  end

  def then_the_file_is_displayed
    expect(page).to have_content(File.read(output_file))
  end

  def when_the_download_button_is_clicked
    visit accounts_path
    find('#accounts_list .fa-download').click
  end

  def then_the_file_is_downloaded
    expect(DownloadHelpers.download_content).to eql(File.read(output_file))
  end

  def when_the_delete_button_is_clicked
    visit accounts_path
    find('#accounts_list .fa-trash').click
  end

  def then_a_message_indicating_account_was_deleted_is_visible
    expect(find('#messages')).to have_content 'Account was successfully destroyed'
  end

  def and_an_empty_table_of_accounts_is_visible
    expect(find('#accounts_list')).to have_content 'No data available'
  end
end

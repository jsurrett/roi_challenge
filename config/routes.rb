Rails.application.routes.draw do
  root to: 'accounts#index'
  resources :accounts, except: [:edit, :update] do
    member do
      get :download
    end
  end
end

# encoding: UTF-8
ActiveRecord::Schema.define(version: 20_160_610_193_440) do
  create_table 'accounts', force: :cascade do |t|
    t.string   'name'
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  add_index 'accounts', ['name'], name: 'index_accounts_on_name'

  create_table 'ad_groups', force: :cascade do |t|
    t.integer 'campaign_id'
    t.string  'name'
    t.decimal 'max_cpc', precision: 4, scale: 2
    t.string  'status'
  end

  add_index 'ad_groups', ['campaign_id'], name: 'index_ad_groups_on_campaign_id'
  add_index 'ad_groups', ['name'], name: 'index_ad_groups_on_name'
  add_index 'ad_groups', ['status'], name: 'index_ad_groups_on_status'

  create_table 'ads', force: :cascade do |t|
    t.integer 'ad_group_id'
    t.string  'headline'
    t.string  'description_line1'
    t.string  'description_line2'
    t.string  'display_url'
    t.string  'final_url'
    t.string  'status'
  end

  add_index 'ads', ['ad_group_id'], name: 'index_ads_on_ad_group_id'
  add_index 'ads', ['headline'], name: 'index_ads_on_headline'
  add_index 'ads', ['status'], name: 'index_ads_on_status'

  create_table 'campaigns', force: :cascade do |t|
    t.integer 'account_id'
    t.string  'name'
    t.decimal 'daily_budget', precision: 6, scale: 2
    t.string  'status'
  end

  add_index 'campaigns', ['account_id'], name: 'index_campaigns_on_account_id'
  add_index 'campaigns', ['name'], name: 'index_campaigns_on_name'
  add_index 'campaigns', ['status'], name: 'index_campaigns_on_status'

  create_table 'keywords', force: :cascade do |t|
    t.integer 'ad_group_id'
    t.string  'keyword'
    t.string  'criterion_type'
    t.decimal 'first_page_bid', precision: 4, scale: 2
    t.string  'status'
  end

  add_index 'keywords', ['ad_group_id'], name: 'index_keywords_on_ad_group_id'
  add_index 'keywords', ['criterion_type'], name: 'index_keywords_on_criterion_type'
  add_index 'keywords', ['keyword'], name: 'index_keywords_on_keyword'
  add_index 'keywords', ['status'], name: 'index_keywords_on_status'
end

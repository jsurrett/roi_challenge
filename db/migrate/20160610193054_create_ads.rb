class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.references :ad_group, index: true, foreign_key: true
      t.string :headline
      t.string :description_line1
      t.string :description_line2
      t.string :display_url
      t.string :final_url
      t.string :status
    end
    add_index :ads, :headline
    add_index :ads, :status
  end
end

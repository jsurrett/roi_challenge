class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.references :account, index: true, foreign_key: true
      t.string :name
      t.decimal :daily_budget, precision: 6, scale: 2
      t.string :status
    end
    add_index :campaigns, :name
    add_index :campaigns, :status
  end
end

class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.references :ad_group, index: true, foreign_key: true
      t.string :keyword
      t.string :criterion_type
      t.decimal :first_page_bid, precision: 4, scale: 2
      t.string :status
    end
    add_index :keywords, :keyword
    add_index :keywords, :criterion_type
    add_index :keywords, :status
  end
end

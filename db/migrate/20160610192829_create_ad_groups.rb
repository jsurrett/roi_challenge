class CreateAdGroups < ActiveRecord::Migration
  def change
    create_table :ad_groups do |t|
      t.references :campaign, index: true, foreign_key: true
      t.string :name
      t.decimal :max_cpc, precision: 4, scale: 2
      t.string :status
    end
    add_index :ad_groups, :name
    add_index :ad_groups, :status
  end
end

# run with bin/rake convert_adwords_to_json account='sample' input='input.tsv' output='output.json'

task convert_adwords_to_json: :environment do
  account = ENV['account']
  input = ENV['input']
  output = ENV['output']
  Convert.run!(
    account_name: account,
    input_file: input,
    output_file: output
  )
end

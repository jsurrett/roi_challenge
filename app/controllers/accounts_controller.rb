class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :download, :destroy]

  def index
    @accounts = Account.all
    @account = Account.new
  end

  def new
    @account = Account.new
  end

  def create
    @account = Account.create(name: params[:account][:name])
    file_name = "#{Rails.root}/import/#{@account.name}.tsv"
    File.open(file_name, 'wb') { |f| f.write(params[:account][:file].read) }

    if @account.errors.none? && File.exist?(file_name)
      ImportAccount.run!(account: @account, file: file_name)
      redirect_to accounts_path, notice: "#{@account.name} Account was successfully created."
    else
      errors = @account.errors.messages.map { |k, v| "#{k.to_s.titleize} #{v.join(', ')}." }.join(' ').to_s
      redirect_to accounts_path, flash: { danger: "The account was not able to be created. #{errors}" }
    end
  end

  def show
    json = JSON.parse(::AccountSerializer.new(@account).to_json)
    render json: JSON.pretty_generate(json) + "\n"
  end

  def download
    json = JSON.parse(::AccountSerializer.new(@account).to_json)
    send_data JSON.pretty_generate(json) + "\n", filename: 'output.json', type: 'text/json'
  end

  def destroy
    file_name = "#{Rails.root}/import/#{@account.name}.tsv"
    File.delete(file_name) if File.exist?(file_name)
    @account.destroy
    redirect_to accounts_url, notice: 'Account was successfully destroyed.'
  end

  private

  def set_account
    @account = Account.find(params[:id])
  end
end

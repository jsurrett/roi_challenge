class AccountSerializer < ActiveModel::Serializer
  self.root = false
  has_many :campaigns

  def campaigns
    object.campaigns.order(:name)
  end
end

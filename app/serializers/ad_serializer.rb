class AdSerializer < ActiveModel::Serializer
  attributes :headline
  attributes :description_line1
  attributes :description_line2
  attributes :display_url
  attributes :final_url
  attributes :status
end

class CampaignSerializer < ActiveModel::Serializer
  attributes :name, :daily_budget, :status
  has_many :ad_groups

  def daily_budget
    "%g" % object.daily_budget
  end

  def ad_groups
    object.ad_groups.order(:name)
  end
end

class AdGroupSerializer < ActiveModel::Serializer
  attributes :name, :max_cpc, :status
  has_many :ads
  has_many :keywords

  def ads
    object.ads.order(headline: :asc, description_line1: :asc, description_line2: :asc)
  end

  def keywords
    object.keywords.order(keyword: :asc, criterion_type: :asc)
  end
end

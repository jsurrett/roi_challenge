class KeywordSerializer < ActiveModel::Serializer
  attributes :keyword
  attributes :type
  attributes :first_page_bid
  attributes :status

  def first_page_bid
    "%g" % object.first_page_bid
  end

  def type
    object.criterion_type
  end
end

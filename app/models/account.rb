class Account < ActiveRecord::Base
  has_many :campaigns, dependent: :destroy

  validates :name, presence: true, uniqueness: true
end

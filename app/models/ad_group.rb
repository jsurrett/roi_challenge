class AdGroup < ActiveRecord::Base
  # Associations
  belongs_to :campaign
  has_many :ads, dependent: :destroy
  has_many :keywords, dependent: :destroy

  # Validations
  validates :campaign, presence: true
  validates :name, presence: true
  validates :max_cpc, numericality: { greater_than_or_equal_to: 0 }
  validates :status, presence: true
end

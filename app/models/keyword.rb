class Keyword < ActiveRecord::Base
  # Associations
  belongs_to :ad_group

  # Validations
  validates :ad_group, presence: true
  validates :keyword, presence: true
  validates :criterion_type, presence: true
  validates :first_page_bid, numericality: { greater_than_or_equal_to: 0 }
  validates :status, presence: true
end

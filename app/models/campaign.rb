class Campaign < ActiveRecord::Base
  # Associations
  belongs_to :account
  has_many :ad_groups, dependent: :destroy

  # Validations
  validates :account, presence: true
  validates :name, presence: true
  validates :daily_budget, numericality: { greater_than_or_equal_to: 0 }
  validates :status, presence: true
end

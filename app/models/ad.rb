class Ad < ActiveRecord::Base
  # Associations
  belongs_to :ad_group

  # Validations
  validates :ad_group, presence: true
  validates :headline, presence: true
  validates :description_line1, presence: true
  validates :description_line2, presence: true
  validates :display_url, url: true
  validates :final_url, url: true
  validates :status, presence: true
end

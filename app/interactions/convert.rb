class Convert < ActiveInteraction::Base
  string :input_file
  string :output_file
  string :account_name, default: 'main'
  #
  def execute
    @account = Account.find_or_create_by(name: account_name)
    @account.campaigns.destroy_all if @account.campaigns.any?
    ImportAccount.run!(account: @account, file: input_file)
    ExportAccount.run!(account: @account, file: output_file)
  end
end

class ExportAccount < ActiveInteraction::Base
  object :account
  string :file

  def execute
    json = JSON.parse(::AccountSerializer.new(account).to_json)
    File.open(file, 'w+') { |f| f.write JSON.pretty_generate(json) + "\n" }
  end
end

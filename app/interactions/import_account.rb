require 'csv'

class ImportAccount < ActiveInteraction::Base
  object :account
  string :file
  #
  def execute
    account.campaigns.destroy_all if account.campaigns.any?

    campaign = nil
    ad_group = nil
    CSV.foreach(file, col_sep: "\t", headers: true) do |row|
      if row['Campaign'].present?
        campaign = Campaign.find_or_initialize_by(account: account, name: row['Campaign'])
        campaign.daily_budget = row['Campaign Daily Budget'] if row['Campaign Daily Budget']
        campaign.status = row['Campaign Status'] if row['Campaign Status']
        campaign.save! if campaign.changed?
      end
      if row['Ad Group'].present?
        ad_group = create_ad_group(campaign, row)
      end
      create_ad(ad_group, row) if row['Headline'].present?
      create_keyword(ad_group, row) if row['Keyword'].present?
    end
  end

  def create_ad_group(campaign, row)
    ad_group = AdGroup.find_or_initialize_by(campaign: campaign, name: row['Ad Group'])
    ad_group.max_cpc = BigDecimal(row['Max CPC']) if row['Max CPC']
    ad_group.status = row['Ad Group Status'] if row['Ad Group Status']
    ad_group.save! if ad_group.changed?
    ad_group
  end

  def create_ad(ad_group, row)
    Ad.create(
      ad_group: ad_group,
      headline: row['Headline'],
      description_line1: row['Description Line 1'],
      description_line2: row['Description Line 2'],
      display_url: row['Display URL'],
      final_url: row['Final URL'],
      status: row['Status']
    )
  end

  def create_keyword(ad_group, row)
    Keyword.find_or_create_by(
      ad_group: ad_group,
      keyword: row['Keyword'],
      criterion_type: row['Criterion Type'],
      first_page_bid: row['First page bid'],
      status: row['Status']
    )
  end
end
